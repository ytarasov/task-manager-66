package ru.t1.ytarasov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.dto.request.project.ProjectUpdateByIdRequest;
import ru.t1.ytarasov.tm.event.ConsoleEvent;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;
import ru.t1.ytarasov.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-update-by-id";

    @NotNull
    public static final String DESCRIPTION = "Update project by index";

    @Override
    @EventListener(condition = "@projectUpdateByIdListener.getName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) throws Exception {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(getToken());
        request.setProjectId(id);
        request.setName(name);
        request.setDescription(description);
        @Nullable final ProjectDTO project = getProjectEndpoint().updateProjectById(request).getProject();
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

}
