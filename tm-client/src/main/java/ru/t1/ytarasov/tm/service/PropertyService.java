package ru.t1.ytarasov.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    @Value("#{environment['server.host']}")
    private String host;

    @NotNull
    @Value("#{environment['server.port']}")
    private String port;

}
