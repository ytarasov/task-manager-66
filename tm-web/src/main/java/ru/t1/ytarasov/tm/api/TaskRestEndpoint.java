package ru.t1.ytarasov.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.model.dto.TaskDto;

import java.util.Collection;
import java.util.List;

@RequestMapping("/api/tasks")
public interface TaskRestEndpoint {

    @PutMapping("/create")
    void create();

    @PostMapping("/save")
    void save(@RequestBody TaskDto task);

    @GetMapping("/findAll")
    Collection<TaskDto> findAll();

    @GetMapping("/findById/{id}")
    TaskDto findById(@PathVariable("id") String id);

    @GetMapping("/count")
    long count();

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @PostMapping("/delete")
    void delete(@RequestBody TaskDto task);

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("/deleteAll")
    void deleteAll(@RequestBody List<TaskDto> tasks);

    @DeleteMapping("/clear")
    void clear();

}
