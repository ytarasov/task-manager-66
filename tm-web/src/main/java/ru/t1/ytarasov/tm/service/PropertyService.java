package ru.t1.ytarasov.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @NotNull
    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @NotNull
    @Value("#{environment['session.key']}")
    private String sessionKey;

    @NotNull
    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @NotNull
    @Value("#{environment['database.url']}")
    private String dbUrl;

    @NotNull
    @Value("#{environment['database.username']}")
    private String dbUsername;

    @NotNull
    @Value("#{environment['database.password']}")
    private String dbPassword;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String dbDriver;

    @NotNull
    @Value("#{environment['database.dialect']}")
    private String dbDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl_auto']}")
    private String dbHbm2ddlAuto;

    @NotNull
    @Value("#{environment['database.show_sql']}")
    private String showSql;

    @NotNull
    @Value("#{environment['database.format_sql']}")
    private String formatSql;

    @NotNull
    @Value("#{environment['database.second_lvl_cache']}")
    private String secondLvlCash;

    @NotNull
    @Value("#{environment['database.factory_class']}")
    private String factoryClass;

    @NotNull
    @Value("#{environment['database.use_query_cache']}")
    private String useQueryCash;

    @NotNull
    @Value("#{environment['database.use_min_puts']}")
    private String useMinPuts;

    @NotNull
    @Value("#{environment['database.region_prefix']}")
    private String regionPrefix;

    @NotNull
    @Value("#{environment['database.config_file_path']}")
    private String configFilePath;

}
